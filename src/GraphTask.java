/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */

/**
 * lk. 20, ül. 15 - koostada meetod etteantud sidusa lihtgraafi G diameetri leidmiseks.
 * Shortest path between each pair of vertices: Floyd-Warshalli algorithm - O(|V|3)
 * The diameter d of a graph is the maximum eccentricity of any vertex in the graph.
 * d is the greatest distance between any pair of vertices or, alternatively, d = max v ∈ V ϵ ( v ).
 * To find the diameter of a graph, first find the shortest path between each pair of vertices.
 * The greatest length of any of these paths is the diameter of the graph.
 * <p>
 * code snippets used from examples: http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
 */

public class GraphTask {

  /**
   * Main method.
   */
  public static void main(String[] args) {
    GraphTask a = new GraphTask();
    a.run();
  }

  /**
   * Actual main method to run examples and everything.
   */
  public void run() {
    Graph g = new Graph("G");
    g.createRandomSimpleGraph(2000, 2000);
    System.out.println(g);
    g.shortestPaths(g.getDistMatrix(g.createAdjMatrix()));
  }

  class Vertex {

    private String id;
    private Vertex next;
    private Arc first;
    private int info = 0;
    // You can add more fields, if needed

    Vertex(String s, Vertex v, Arc e) {
      id = s;
      next = v;
      first = e;
    }

    Vertex(String s) {
      this(s, null, null);
    }

    @Override
    public String toString() {
      return id;
    }
  }


  /**
   * Arc represents one arrow in the graph. Two-directional edges are
   * represented by two Arc objects (for both directions).
   */
  class Arc {

    private String id;
    private Vertex target;
    private Arc next;
    private int info = 0;

    Arc(String s, Vertex v, Arc a) {
      id = s;
      target = v;
      next = a;
    }

    Arc(String s) {
      this(s, null, null);
    }

    @Override
    public String toString() {
      return id;
    }
  }


  class Graph {

    private String id;
    private Vertex first;
    private int info = 0;

    Graph(String s, Vertex v) {
      id = s;
      first = v;
    }

    Graph(String s) {
      this(s, null);
    }

    @Override
    public String toString() {
      String nl = System.getProperty("line.separator");
      StringBuffer sb = new StringBuffer(nl);
      sb.append(id);
      sb.append(nl);
      Vertex v = first;

      while (v != null) {
        sb.append(v.toString());
        sb.append(" -->");
        Arc a = v.first;

        while (a != null) {
          sb.append(" ");
          sb.append(a.toString());
          sb.append(" (");
          sb.append(v.toString());
          sb.append("->");
          sb.append(a.target.toString());
          sb.append(")");
          a = a.next;
        }
        sb.append(nl);
        v = v.next;
      }

      return sb.toString();
    }

    public Vertex createVertex(String vid) {
      Vertex res = new Vertex(vid);
      res.next = first;
      first = res;

      return res;
    }

    public Arc createArc(String aid, Vertex from, Vertex to) {
      Arc res = new Arc(aid);
      res.next = from.first;
      from.first = res;
      res.target = to;

      return res;
    }

    /**
     * Create a connected undirected random tree with n vertices.
     * Each new vertex is connected to some random existing vertex.
     *
     * @param n number of vertices added to this graph
     */
    public void createRandomTree(int n) {
      if (n <= 0)
        return;
      Vertex[] varray = new Vertex[n];

      for (int i = 0; i < n; i++) {
        varray[i] = createVertex("v" + String.valueOf(n - i));
        if (i > 0) {
          int vnr = (int) (Math.random() * i);
          createArc("a" + varray[vnr].toString() + "_"
              + varray[i].toString(), varray[vnr], varray[i]);
          createArc("a" + varray[i].toString() + "_"
              + varray[vnr].toString(), varray[i], varray[vnr]);
        } else {
        }
      }
    }

    /**
     * Create an adjacency matrix of this graph.
     * Side effect: corrupts info fields in the graph
     *
     * @return adjacency matrix
     */
    public int[][] createAdjMatrix() {
      info = 0;
      Vertex v = first;
      while (v != null) {
        v.info = info++;
        v = v.next;
      }
      int[][] res = new int[info][info];
      v = first;

      while (v != null) {
        int i = v.info;
        Arc a = v.first;
        while (a != null) {
          int j = a.target.info;
          res[i][j]++;
          a = a.next;
        }
        v = v.next;
      }

      return res;
    }

    /**
     * Create a connected simple (undirected, no loops, no multiple
     * arcs) random graph with n vertices and m edges.
     *
     * @param n number of vertices
     * @param m number of edges
     */
    public void createRandomSimpleGraph(int n, int m) {
      if (n <= 0)
        return;
      if (n > 2500)
        throw new IllegalArgumentException("Too many vertices: " + n);
      if (m < n - 1 || m > n * (n - 1) / 2)
        throw new IllegalArgumentException
            ("Impossible number of edges: " + m);
      first = null;
      createRandomTree(n);                  // n-1 edges created here
      Vertex[] vert = new Vertex[n];
      Vertex v = first;
      int c = 0;
      while (v != null) {
        vert[c++] = v;
        v = v.next;
      }
      int[][] connected = createAdjMatrix(); //returns: number of arcs from start vertex to end vertex
      int edgeCount = m - n + 1;             // remaining edges

      while (edgeCount > 0) {
        int i = (int) (Math.random() * n);     // random source
        int j = (int) (Math.random() * n);     // random target
        if (i == j)
          continue;                        // no loops
        if (connected[i][j] != 0 || connected[j][i] != 0)
          continue;                        // no multiple edges
        Vertex vi = vert[i];
        Vertex vj = vert[j];
        createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
        connected[i][j] = 1;
        createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
        connected[j][i] = 1;
        edgeCount--;                        // a new edge happily created
      }
    }

    /**
     * Converts adjacency matrix to the matrix of distances.
     *
     * @param adjMatrix adjacency matrix
     * @return matrix of distances, where each edge has length 1
     */

    public int[][] getDistMatrix(int[][] adjMatrix) {        // matrix is two dimensional array -> [][]
      int numOfVerts = adjMatrix.length;                    // number of vertices should be adjacent matrix length
      int[][] distMatrix = new int[numOfVerts][numOfVerts]; // create new distance matrix[][]

      if (numOfVerts < 1) return distMatrix;
      int INFINITY = 2 * numOfVerts + 1;                    // Integer.MAX_VALUE / 2; // NB!!!

      for (int i = 0; i < numOfVerts; i++) {
        for (int j = 0; j < numOfVerts; j++) {
          if (adjMatrix[i][j] == 0) {                     // no connection
            distMatrix[i][j] = INFINITY;
          } else {
            distMatrix[i][j] = 1;
          }
        }
      }
      for (int i = 0; i < numOfVerts; i++) {                    // eliminate loops
        distMatrix[i][i] = 0;
      }

      return distMatrix;
    }

    /**
     * Calculates shortest paths using Floyd-Warshall algorithm.
     * This matrix will contain shortest paths between each pair of vertices.
     */

    public void shortestPaths(int[][] distMatrix) {      // calculate from matrix [][]
      long startTime, finishTime, diff;
      int n = distMatrix.length;                         // number of vertices -> same as adjacency matrix length.

      if (n < 1) return;
      for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
          for (int j = 0; j < n; j++) {
            int newLength = distMatrix[i][k] + distMatrix[k][j];

            if (distMatrix[i][j] > newLength) {
              distMatrix[i][j] = newLength;         // new path is shorter
            }
          }
        }
      }
      System.out.println("Shortest paths matrix between each pair of vertices" + "\n");

      startTime = System.nanoTime();
      printDistMatrix(distMatrix);
      finishTime = System.nanoTime();
      diff = finishTime - startTime;

      System.out.printf("%34s%11d%n", "Graph (2000 nodes): time (ms): ", diff / 1000000);
      System.out.println("\n" + "Graph Diameter: " + getDiameter(distMatrix));
    }

    /**
     * Prints shortest paths matrix between each pair of vertices
     *
     * @param distMatrix
     */

    public void printDistMatrix(int distMatrix[][]) {    // Shortest paths matrix between each pair of vertices
      int numOfVertices = distMatrix.length;            // Number of vertices given in in createRandomSimpleGraph (int n, int m)

      for (int i = 0; i < numOfVertices; i++) {
        for (int j = 0; j < numOfVertices; j++) {
          System.out.print(distMatrix[i][j] + "  ");    // Print out every number in given location
        }
        System.out.println();                           // New line after each i row
      }
    }

    /**
     * Get maximum distance from matrix -> Diameter
     * https://www.programiz.com/java-programming/multidimensional-array
     *
     * @param array
     * @return maxDistance
     */

    public int getDiameter(int[][] array) {
      int maxDistance = 0;

      for (int[] innerArray : array) {
        for (int distance : innerArray) {
          if (distance > maxDistance) {
            maxDistance = distance;
          }
        }
      }

      return maxDistance;
    }
  }
}